from http import HTTPStatus

from flask import Blueprint, render_template, url_for, redirect, request
import copy

from app.frase.models import crear_frase, eliminar_frase, obtener_todas

frase = Blueprint("frase", __name__, url_prefix="/frase")

RESPONSE_BODY_DEFAULT = {"message": "", "data": [], "errors": [], "metadata": []}


@frase.route("/", methods=["GET"])
def index():
    frases = obtener_todas()
    data = {
        "frases":frases
    }
    return render_template('frase/index.html', data=frases)


@frase.route("/agregar", methods=["POST","GET"])
def agregar():
    response_body = copy.deepcopy(RESPONSE_BODY_DEFAULT)
    status_code = HTTPStatus.OK
    if request.method == "POST":

        fra_v_frase = request.form["fra_v_frase"]
        fra_v_categoria = request.form["fra_v_categoria"]


        if fra_v_frase == "":
            response_body["errors"].append("Campo frase es requerido")
            status_code = HTTPStatus.BAD_REQUEST
            return response_body, status_code

        if fra_v_categoria == "":
            response_body["errors"].append("Campo categoria es requerido")
            status_code = HTTPStatus.BAD_REQUEST
            return response_body, status_code

        frase = crear_frase(fra_v_frase, fra_v_categoria)

        return render_template('frase/agregar.html')
    else:

        return render_template('frase/agregar.html')


@frase.route("/eliminar/<int:id>", methods=["GET"])
def eliminar(id):
    response_body = copy.deepcopy(RESPONSE_BODY_DEFAULT)
    status_code = HTTPStatus.BAD_REQUEST
    if id != "":

        if eliminar_frase(id):
            return redirect(url_for('frase.index'))
        else:
            response_body["errors"].append("Error al eliminar la frase")
            return response_body, status_code

    else:
        response_body["errors"].append("El id es requerido, para eliminar la frase")
        return response_body, status_code


