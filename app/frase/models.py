from app.db import db, ma


class Frase(db.Model):
    fra_i_id = db.Column(db.Integer, primary_key=True)
    fra_v_frase = db.Column(db.String(45), nullable=False)
    fra_v_categoria = db.Column(db.String(45), nullable=False)

class FraseSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Frase
        fields = ["fra_i_id", "fra_v_frase", "fra_v_categoria"]

def obtener_frases_por_categoria(categoria):
    frases_filtro = Frase.query.filter_by(fra_v_categoria=categoria)
    if frases_filtro != None:
        frase_schema = FraseSchema()
        frases = [frase_schema.dump(f) for f in frases_filtro]
        return frases
    else:
        return None

def obtener_todas():
    frases = Frase.query.all()
    if frases != None:
        frase_schema = FraseSchema()
        frases = [frase_schema.dump(f) for f in frases]
        return frases
    else:
        return None




def crear_frase(frase, categoria):
    frase = Frase(fra_v_frase=frase, fra_v_categoria = categoria)
    db.session.add(frase)
    db.session.commit()
    frase_schema = FraseSchema()
    return frase_schema.dump(frase)

def frase_id(id):
    frase = Frase.query.filter_by(fra_i_id=id).first()
    if frase != None:
        frase_schema = FraseSchema()
        return frase_schema.dump(frase)
    else:
        return ''

def eliminar_frase(id):
    frase = Frase.query.filter_by(fra_i_id=id).first()
    if frase != None:
        Frase.query.filter_by(fra_i_id=id).delete()
        db.session.commit()
        return True
    else:
        return False
