from app.frase.models import obtener_frases_por_categoria, obtener_todas, frase_id
from datetime import datetime
import random

class ChatBot():
    def __init__(self):
        self.contexto = []

    def responder(self,frase):
        now = datetime.now()
        respuesta = ''
        categoria = ''
        dt_string = now.strftime("%H:%M")
        frases = obtener_todas()
        frase2 = frase
        frases2 = [frase['fra_v_frase'].lower() for frase in frases]
        for e in ['?','¿','!','¡',',']:
            frases2 = [frase.replace(e,'') for frase in frases2]
            frase2 = frase2.replace(e,'')
        for i in range(len(frases)):
            frases[i]['fra_v_frase'] = frases2[i]
        if frase2.lower() not in frases2:
            categoria = 'Default'
        else:
            for f in frases:
                if f['fra_v_frase'].lower() == frase2.lower():
                    categoria = f['fra_v_categoria']
        
    
        if categoria == 'Pregunta':
            respuestas = obtener_frases_por_categoria('Respuesta')
            respuesta = random.choice(respuestas)
            respuesta = respuesta['fra_v_frase']
        elif categoria == 'Pregunta-SN':
            respuestas = obtener_frases_por_categoria('Respuesta-SN')
            respuesta = random.choice(respuestas)
            respuesta = respuesta['fra_v_frase']
        elif categoria == 'Pregunta-lugar':
            respuestas = obtener_frases_por_categoria('Respuesta-lugar')
            respuesta = random.choice(respuestas)
            respuesta = respuesta['fra_v_frase']
        elif categoria == 'Pregunta-h':
            respuestas = obtener_frases_por_categoria('Respuesta-h')
            respuesta = random.choice(respuestas)
            respuesta = respuesta['fra_v_frase']
        else:
            respuestas = obtener_frases_por_categoria(categoria)
            respuesta = random.choice(respuestas)
            respuesta = respuesta['fra_v_frase']
        
        self.add_contexto({'user': frase, 'bot': respuesta, 'hora': dt_string})

    def add_contexto(self, ctx):
        self.contexto.append(ctx)

    