from flask import Blueprint, render_template, url_for, redirect, request

from app.chatbot.models import ChatBot

chat = Blueprint("chat", __name__, url_prefix="/chat")
bot = ChatBot()

@chat.route("/", methods=["GET"])
def index():
    return render_template('chatbot/index.html')


@chat.route("/responder", methods=["POST"])
def responder():
    frase = request.form['frase']
    if frase.strip != '':
        bot.responder(frase)
        return render_template('chatbot/index.html',ctx = bot.contexto)
    else:
        return render_template('chatbot/index.html',ctx = bot.contexto)

